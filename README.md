# Devisr Events
Devisr Events is a hooks-based events library to help with plugin systems.

To listen to an event:

```php
    Events::listen("event.name", function($arguments) {
        // do stuff here
        return [ 1, 2, 3 ]; // optionally pass new arguments to the event target
    });
```

To create and invoke an event:

```php
    (new Event("event.name", function($arguments) {
        // default event actions go here
        return 1; // optionally pass a value to the event.name:end event 
    }))($arguments) // pass default event arguments
```