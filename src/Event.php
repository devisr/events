<?php

namespace Devisr\Events;

use \Closure;

/**
 * Simple hooks-based events system for the Devisr framework
 * Used for plugin creation
 * 
 * @author Cythral <talen.fisher@cythral.com>
 */
class Event {
    static private $listeners;
    private $name;
    private $action;
    private $mgr;

    /**
     * Creates a new Event Object
     *
     * @param string $name
     * @param Closure $action
     * @param object|null $mgr
     */
    public function __construct(string $name, Closure $action, ?object $mgr = null) {
        $this->name = $name;
        $this->action = $action;
        $this->mgr = $mgr ?? $this;

        // create listener arrays if not there
        foreach([ $this->name, "{$this->name}:end" ] as $event) {
            if(!isset(self::$listeners[$event])) self::$listeners[$event] = [];
        }
    }

    /**
     * Invokes the Event action
     *
     * @return void
     */
    public function __invoke(...$args): void {
        foreach(self::$listeners[$this->name] as $listener) {
            static $last = null;
            $last = $listener->call($this->mgr, ...$args);
        }

        $args = $last ?? $args;
        $this->invokeEnd(array_merge($args, [ $this->action->call($this->mgr, ...$args) ]));
    }

    /**
     * Helper method for invoke, calls event:end listeners
     *
     * @param array $args an array of arguments to pass to listeners.  last argument should be the return value of the event
     * @return void
     */
    private function invokeEnd($args): void {
        foreach(self::$listeners["{$this->name}:end"] as $listener) {
            $listener->call($this->mgr, ...$args);
        }
    }

    /**
     * Listens for an event
     *
     * @param string $name the name of the event to listen for
     * @param Closure $listener function to call when the event occurs
     * @return void
     */
    static public function listen(string $name, Closure $listener): void {
        if(!isset(self::$listeners[$name])) self::$listeners[$name] = [];
        self::$listeners[$name][] = $listener;
    }
}