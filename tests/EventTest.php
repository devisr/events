<?php

use Devisr\Events\Event;
use PHPUnit\Framework\TestCase;

class EventTest extends TestCase {
    public function testListenNoPassthroughArguments() {
        Event::listen("test", function() {});

        (new Event("test", function($a) {
            $this->assertEquals("a", $a);
        }, $this))("a");
    }

    public function testListenPassthroughArguments() {
        Event::listen("test", function() {
            return [ "b" ];
        });

        (new Event("test", function($a) {
            $this->assertEquals("b", $a);
        }, $this))("a");
    }

    public function testListenMultiplePassthroughArguments() {
        Event::listen("test", function() {
            return [ "b" ];
        });

        Event::listen("test", function() {
            return [ "c" ];
        });

        (new Event("test", function($a) {
            $this->assertEquals("c", $a);
        }, $this))("a");
    }
}